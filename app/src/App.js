import React from 'react';
import './App.scss';
import { LoginPage } from './components/login/index';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import GroupDisplay from './pages/GroupDisplay';

export default function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <LoginPage />
          </Route>
          <Route exact path="/login">
            <LoginPage />
          </Route>
          <Route path="/browse">
              <GroupDisplay />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}
