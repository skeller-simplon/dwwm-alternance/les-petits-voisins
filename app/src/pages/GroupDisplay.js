import React from "react";
import Container from "react-bootstrap/Container"
import NewHelp from "../components/help/NewHelp";
import HelpList from "../components/help/HelpList";

function GroupDisplay(){
    return(
        <Container>
            <NewHelp />
            <HelpList />
        </Container>
    )
}

export default GroupDisplay