import BaseService from './baseService';
import axios from "axios";

export default class CommentService extends BaseService{
    helpsApiUrl = this.baseApiUrl + "/comments";
    createComment(helpId, paragraph ){
        this.getConnectedUser().then(user => {
            const data = {
                help: helpId,
                author: user["@id"],
                paragraph: paragraph
              }
            return axios.post(this.helpsApiUrl, data)
        })
    }
}
