import BaseService from './baseService';
import axios from "axios";

export default class UserService extends BaseService {
    userApiUrl = this.baseApiUrl + "/users";
    /**
     * Route de login, enregistre le JWT token en local storage.
     * @param {string} email Email de l'utilisateur
     * @param {string} password Mot de passe de l'utilisateur
     */
    loginUser(email, password) {
        const body = {
            email: email,
            password: password,
        }
        // Fix -> supprime le token du local storage avant la requête.
        this.logoutUser()
        return axios.post(this.baseApiUrl + '/login', body).then(res => {
            localStorage.setItem('token', res.data.token);
            return true
        }).catch(err => { return false });
    }

    logoutUser(){
        localStorage.removeItem("token")
    }
    /**
     * Récupères l'utilisateur connecté.
     */
    
    getAllUsers(){
        return axios.get(this.userApiUrl).then(res => res.data['hydra:member'])
    }

    createUser(username, address, email, password) {
        const body = {
            username: username,
            address: address,
            email: email,
            password: password,
        }
        return axios.post(this.baseApiUrl + '/users', body).then(res => res.data['hydra:member'])
    }
}
