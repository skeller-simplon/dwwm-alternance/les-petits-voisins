import axios from "axios";

export default class BaseService{
    baseApiUrl = "http://127.0.0.1:8000/api";

    getConnectedUser(){
        return axios.get(this.baseApiUrl + "/getConnectedUser").then(res => {
            return {...res.data, "@id": this.removeRouteFromApiPlatformId(res.data["@id"])}
        })
    }

    parseJwt = (token) => {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };

    cleanAllUserIds(obj){
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (typeof obj[property] == "object") {
                    this.cleanAllUserIds(obj[property]);
                }
                else {
                    if (property === "@id"){
                        obj[property] = this.removeRouteFromApiPlatformId(obj[property])
                    }
                }
            }
        }
        return obj
    }

    /**
     * Transforme /api/getConnectedUser?id=8e912b8a-78db-4516-98e6-2ad5d0709c3f
     * en une id brute exploitable, ici : 8e912b8a-78db-4516-98e6-2ad5d0709c3f 
     * @param {*} routeAndId 
     */
    removeRouteFromApiPlatformId(routeAndId){
        if(routeAndId.indexOf("=") === -1)
            return routeAndId.substring(routeAndId.lastIndexOf("/") + 1)
        return routeAndId.substring(routeAndId.indexOf("=") + 1)
    }
}
