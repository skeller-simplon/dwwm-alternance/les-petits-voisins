import BaseService from './baseService';

import axios from "axios";

export default class HelpService extends BaseService{
    helpsApiUrl = this.baseApiUrl + "/helps";
    getAllHelps(){
        return axios.get(this.helpsApiUrl).then(res => {
            let cleanedObj = this.cleanAllUserIds(res.data["hydra:member"])
            console.log(cleanedObj)
            return res.data["hydra:member"];
        })
    }
    createHelp(titre, paragraph, type){
        return this.getConnectedUser().then(user => {
            console.log(user)
            const data = {
                "titre": titre,
                "paragraph": paragraph,
                "status": true,
                "type": type,
                "comments": []
              }
            return axios.post(this.helpsApiUrl, data)
        })
    }
        
}
