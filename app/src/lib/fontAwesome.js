import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faUserCircle, faCaretDown, faComments} from '@fortawesome/free-solid-svg-icons'
 
library.add(faUser, faUserCircle, faCaretDown, faComments)