import React, { useState, useEffect } from 'react';
import HelpService from "../../Services/helpService"
import Help from './Help';
export default function HelpList(){
  const [helpService] = useState(new HelpService())
  const [helps, setHelps] = useState([]);
  
  useEffect(() => {
    helpService.getAllHelps().then(response => {
      setHelps(response)
    })
  }, [helpService]);

  return (
      <>
      {helps && 
          helps.map(help => (
            <Help key={help["@id"]} help={help}></Help>
          ))
      }
      </>
  );
}
