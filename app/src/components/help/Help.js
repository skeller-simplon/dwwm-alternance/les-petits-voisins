import React from 'react';
import Card from 'react-bootstrap/Card'
import CommentsBuilder from '../comment/CommentsBuilder';

function Help({help}) {
    return(
        <Card className="text-left mt-2">
            <Card.Body>
              <Card.Title>{help.titre} &nbsp; <small className="text-muted">Créé par {help.author.username}</small></Card.Title>
              <Card.Text>
                {help.paragraph}
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <CommentsBuilder help={help} />
            </Card.Footer>
        </Card>
    )}


export default Help;
