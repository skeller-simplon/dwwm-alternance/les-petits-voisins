import React, { useState } from 'react';
import Jumbotron from "react-bootstrap/Jumbotron"
import Form from "react-bootstrap/Form"
import Col from "react-bootstrap/Col"
import Button from "react-bootstrap/Button"
import Container from "react-bootstrap/Container"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import HelpTypeLib from "../../lib/HelpTypeLib"
import HelpService from "../../Services/helpService"

function NewHelp() {
    const [formValue, setFormValue] = useState({titre: "", paragraph: "", type:HelpTypeLib[1]})
    const [helpService] = useState(new HelpService());
    
    const onTypeChange = typeId => {
        setFormValue({
            ...formValue, 
            type: HelpTypeLib[typeId]
        })
    }
    const submitForm = () => {
        helpService.createHelp(formValue.titre, formValue.paragraph, formValue.type)
    }

    return (
        <Jumbotron className="bg-success-light my-3 pb-6 pt-4 text-left">
            <Container>
                <ul className="list-inline">
                    <li className="list-inline-item">
                        <h4>
                            <FontAwesomeIcon icon="user"/>&nbsp;SamuelKeller
                        </h4>
                    </li>
                    <li className="list-inline-item"><h6>Partagez un message, un évènement ou un besoin à votre groupe.</h6></li>
                </ul>
                <Form>
                    <Form.Row>
                        <Form.Group as={Col} md="3" className="my-auto">
                            <Form.Check inline
                                type="radio"
                                label="Je propose"
                                name="formTypeRadios"
                                onChange={() => onTypeChange(1)}
                                defaultChecked={true}
                            />
                            <Form.Check inline
                              type="radio"
                              label="Je recherche"
                              name="formTypeRadios"
                              onChange={() => onTypeChange(2)}
                            />
                        </Form.Group>
                        <Form.Group as={Col} md="9" controlId="helpForm-titre">
                            <Form.Label>Titre</Form.Label>
                            <Form.Control onChange={input => formValue.titre = input.target.value} type="text" rows="3" />
                        </Form.Group>
                    </Form.Row>
                    <Form.Group controlId="helpForm-commentaire">
                        <Form.Label>Commentaire</Form.Label>
                        <Form.Control onChange={input => formValue.paragraph = input.target.value} as="textarea" rows="3" />
                    </Form.Group>
                    <Button onClick={submitForm} className="float-right" variant="light">Publier</Button>
                </Form>
            </Container>
        </Jumbotron>
    );
}


export default NewHelp;
