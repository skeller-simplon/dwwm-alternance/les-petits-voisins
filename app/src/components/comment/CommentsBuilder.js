import React, { useState } from 'react';
import CommentList from "../comment/CommentList";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button"
import CommentService from '../../Services/commentService';

function CommentsBuilder({help}) {
    const [comment, setComment] = useState("Je peut t'aider !")
    const [commentService] = useState(new CommentService())
    const submitForm = () => {
        commentService.createComment(help["@id"], comment)
    }

    return(
        <>
            <h3 className="text-center text-success">
                <FontAwesomeIcon icon="comments"/>&nbsp;Commentaires
            </h3>
            {help.comments.length === 0 ?
              <p className="text-muted">Pas encore de commentaires</p> :
              <CommentList commentList={help.comments}/>
            }
            <Form>
              <Form.Group as={Row} controlId={"commentText" + help["@id"]}>
                <Form.Label column sm="2"> Commenter</Form.Label>
                <Col sm="8">
                  <Form.Control onChange={val => setComment(val.target.value) } type="text" placeholder={comment} />
                </Col>
                <Col sm="2" className="text-center">
                    <Button onClick={submitForm} type="button" className="btn-success mw-100">Envoyer</Button>
                    </Col>
              </Form.Group>
            </Form>
        </>
    )
}


export default CommentsBuilder;
