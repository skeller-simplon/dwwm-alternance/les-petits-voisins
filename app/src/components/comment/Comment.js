import React from 'react';
import Media from "react-bootstrap/Media"

export default function Comment({comment}){
  return (
      <>
        <Media as="li">
          <div className="mr-3 text-center text-muted" >
            {comment.author.username}
          </div>
          <Media.Body>{comment.paragraph}</Media.Body>
        </Media>
      </>
  );
}
