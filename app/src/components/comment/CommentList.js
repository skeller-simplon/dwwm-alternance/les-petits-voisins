import React from 'react';
import Comment from "./Comment";
export default function CommentList({commentList}){
  return (
      <>
      {commentList && 
        <ul className="list-unstyled">
          {commentList.map(comment => (
              <Comment key={comment["@id"]} comment={comment}></Comment>
          ))}
        </ul>
      }
      </>
  );
}
