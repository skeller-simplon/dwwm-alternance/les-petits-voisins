import React, { useState } from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import UserService from '../Services/userService';

function CustomNavbar() {
    const [userService] = useState(new UserService());
    const logout = () => userService.logoutUser();
    return (
        <Navbar bg="success" variant="dark">
            <Navbar.Brand href="#home">Les petits voisins</Navbar.Brand>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                </Form>
            <Navbar.Collapse className="justify-content-end">
            <DropdownButton
                alignRight
                drop="down"
                variant="success"
                title={<FontAwesomeIcon icon="user-circle" size="lg"/>}
            >
                <Dropdown.Item href="/login">Login</Dropdown.Item>
                <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
            </DropdownButton>
            </Navbar.Collapse>
        </Navbar>
    );
}
export default CustomNavbar;
