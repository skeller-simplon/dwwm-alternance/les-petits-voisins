import React from 'react';
import loginImg from "../../login1.svg";
import UserService from '../../Services/userService';
import { Redirect } from 'react-router-dom';

export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userService: new UserService(),
            loginForm: { email: "", password: "" },
            redirect: null
        }
    }
    onEmailChange = (event) => this.setState({ email: event.target.value });
    onPasswordChange = (event) => this.setState({ password: event.target.value });
    submitForm = () => {
        this.state.userService.loginUser(this.state.email, this.state.password)
            .then(status => {
                if (status)
                    this.setState({ redirect: "/browse" });
            })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        } else {
            return (

                <div className="base-container" ref={this.props.containerRef}>
                    <div className="header">Login</div>
                    <div className="content">
                        <div className="image">
                            <img src={loginImg}></img>
                        </div>
                        <div className="form">
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input onChange={this.onEmailChange} type="email" name="email" placeholder="email" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input onChange={this.onPasswordChange} type="password" name="password" placeholder="password" />
                            </div>
                        </div>
                    </div>
                    <div className="footer">
                        <button onClick={() => this.submitForm()} type="button" className="btn">Login</button>
                    </div>
                </div>
            )
        }
    }
}

