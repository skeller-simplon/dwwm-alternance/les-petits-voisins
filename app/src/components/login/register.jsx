import React from 'react';
import loginImg from "../../login1.svg";
import UserService from '../../Services/userService';

export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userService: new UserService(),
            registerForm: { username: "", address: "", email: "", password: "" }
        }
    }

    onUsernameChange = (event) => this.setState({ username: event.target.value });
    onAddressChange = (event) => this.setState({ address: event.target.value });
    onEmailChange = (event) => this.setState({ email: event.target.value });
    onPasswordChange = (event) => this.setState({ password: event.target.value });
    submitForm = () => this.state.userService.createUser(this.state.username, this.state.address, this.state.email, this.state.password);

    render() {
        return (
            <div className="base-container" ref={this.props.containerRef}>
                <div className="header">Register</div>
                <div className="content">
                    <div className="image">
                        <img src={loginImg}></img>
                    </div>
                    <div className="form">
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input onChange={this.onUsernameChange} type="text" name="username" placeholder="username" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="address">Address</label>
                            <input onChange={this.onAddressChange} type="address" name="address" placeholder="address" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input onChange={this.onEmailChange} type="email" name="email" placeholder="email" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input onChange={this.onPasswordChange} type="password" name="password" placeholder="password" />
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <button onClick={() => this.submitForm()} type="button" className="btn">Register</button>
                </div>
            </div>
        )
    }
}

