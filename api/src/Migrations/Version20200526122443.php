<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200526122443 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE help (id INT UNSIGNED AUTO_INCREMENT NOT NULL, club_id INT NOT NULL, author_id INT UNSIGNED NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', titre VARCHAR(255) NOT NULL, paragraph LONGTEXT NOT NULL, status TINYINT(1) NOT NULL, type VARCHAR(15) NOT NULL, INDEX IDX_8875CAC61190A32 (club_id), INDEX IDX_8875CACF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE help ADD CONSTRAINT FK_8875CAC61190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE help ADD CONSTRAINT FK_8875CACF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        // $this->addSql('ALTER TABLE user RENAME INDEX idx_8d93d6491ed93d47 TO IDX_8D93D64939141438');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE help');
        // $this->addSql('ALTER TABLE user RENAME INDEX idx_8d93d64939141438 TO IDX_8D93D6491ED93D47');
    }
}
