<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 *
 * @UniqueEntity(fields={"uuid"})
 *
 * @ApiResource(
 *      normalizationContext={"groups"={
 *          "comment:read", "author:read:from-comment"
 *      }},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"comment:read", "author:read:from-comment"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"comment:write", "comment:post"}},
 *          },
 *      },
 *
 *      itemOperations={
 *          "get",
 *          "put" = {
 *              "denormalization_context"={"groups"={"comment:write"}},
 *          },
 *          "delete"
 *      }
 * )
 */
class Comment
{
    /**
     * @ORM\Column(type="guid")
     * @ApiProperty(identifier=true)
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"comment:post"})
     */
    private $uuid;

    /**
     * @ORM\Id()
     * @ApiProperty(identifier=false)
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Help::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"comment:read", "comment:write"})
     * @Assert\NotBlank
     */
    private $help;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"comment:read", "comment:write", "comment:read:from-help"})
     * @Assert\NotBlank
     */
    private $author;

    /**
     * @Groups({"comment:read", "comment:write", "comment:read:from-help"})
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     */
    private $paragraph;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getHelp(): ?Help
    {
        return $this->help;
    }

    public function setHelp(?Help $help): self
    {
        $this->help = $help;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getParagraph(): ?string
    {
        return $this->paragraph;
    }

    public function setParagraph(string $paragraph): self
    {
        $this->paragraph = $paragraph;

        return $this;
    }
}
