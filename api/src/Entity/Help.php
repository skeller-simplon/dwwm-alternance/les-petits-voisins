<?php

namespace App\Entity;

use App\Repository\HelpRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=HelpRepository::class)
 * @UniqueEntity(fields={"uuid"})
 * @ApiResource(
 *      normalizationContext={"groups"=
 *          {"help:read", "comment:read:from-help", "author:read:from-help"}
 *      },
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"help:read", "comment:read:from-help", "author:read:from-help"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"help:write", "help:post"}},
 *          },
 *      },
 *
 *      itemOperations={
 *          "get",
 *          "put" = {
 *              "denormalization_context"={"groups"={"help:write"}},
 *          },
 *          "delete"
 *      }
 * )
 */
class Help
{
    const TYPE_DEMANDE = 'DEMANDE';
    const TYPE_PROPOSITION = 'PROPOSITION';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ApiProperty(identifier=false)
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     * @ApiProperty(identifier=true)
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"user:post"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"help:read", "help:write"})
     * @Assert\NotBlank
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     * @Groups({"help:read", "help:write"})
     * @Assert\NotBlank
     */
    private $paragraph;

    /**
     * @ORM\ManyToOne(targetEntity=Club::class, inversedBy="helps")
     * @ORM\JoinColumn(nullable=false)
     */
    private $club;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"help:read", "help:write"})
     * @Assert\NotBlank
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=15)
     * @Groups({"help:read", "help:write"})
     * @Assert\NotBlank
     * @Assert\Choice({
     *      Help::TYPE_DEMANDE,
     *      Help::TYPE_PROPOSITION
     * })
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="helps")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"help:read", "help:write", "author:read:from-help"})
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="help", orphanRemoval=true)
     * @Groups({"help:read", "help:write"})
     */
    private $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getParagraph(): ?string
    {
        return $this->paragraph;
    }

    public function setParagraph(string $paragraph): self
    {
        $this->paragraph = $paragraph;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(?Club $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setHelp($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getHelp() === $this) {
                $comment->setHelp(null);
            }
        }

        return $this;
    }

}
