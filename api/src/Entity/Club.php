<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\ClubRepository;

/**
 * @ORM\Entity(repositoryClass=ClubRepository::class)
 *
 * @UniqueEntity(fields={"uuid"})
 *
 * @ApiResource(
 *      normalizationContext={"clubs"={"club:read"
 *      }},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"clubs"={"club:read"}}
 *          },
 *          "post"={
 *              "denormalization_context"={"clubs"={"club:write", "club:post"}},
 *          },
 *      },
 *
 *      itemOperations={
 *          "get",
 *          "put" = {
 *              "denormalization_context"={"clubs"={"club:write"}},
 *          },
 *          "delete"
 *      })
 */
class Club
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @ApiProperty(identifier=false)
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     * @ApiProperty(identifier=true)
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"club:post"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"club:read", "club:write"})
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"club:read", "club:write"})
     * @Assert\NotBlank
     */
    private $user_location;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="userClub", orphanRemoval=true)
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Help::class, mappedBy="club", orphanRemoval=true)
     */
    private $helps;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->helps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUserLocation(): ?string
    {
        return $this->user_location;
    }

    public function setUserLocation(string $user_location): self
    {
        $this->user_location = $user_location;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setUserClub($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getUserClub() === $this) {
                $user->setUserClub(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Help[]
     */
    public function getHelps(): Collection
    {
        return $this->helps;
    }

    public function addHelp(Help $help): self
    {
        if (!$this->helps->contains($help)) {
            $this->helps[] = $help;
            $help->setClub($this);
        }

        return $this;
    }

    public function removeHelp(Help $help): self
    {
        if ($this->helps->contains($help)) {
            $this->helps->removeElement($help);
            // set the owning side to null (unless already changed)
            if ($help->getClub() === $this) {
                $help->setClub(null);
            }
        }

        return $this;
    }
}
