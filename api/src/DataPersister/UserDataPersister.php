<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Club;
use App\Entity\User;
use App\Repository\ClubRepository;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserDataPersister implements DataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var ClubRepository
     */
    private $clubRepository;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, ClubRepository $clubRepository)
    {
        $this->encoder = $encoder;
        $this->em = $em;
        $this->clubRepository = $clubRepository;
    }

    public function supports($data): bool
    {
        return $data instanceof User;
    }
    public function persist($data)
    {
        $encoded = $this->encoder->encodePassword($data, $data->getPassword());
        $data->setPassword($encoded);

        if (!$data->getUuid()) {
            $data->setUuid(Uuid::uuid4()->toString());
        }

        try {
            // Ajout du club à l'utilisateur
            $club = $this->clubRepository->findOneBy(
                array("user_location" => $data->getAddress())
            );
            if (!$club) {
                $club = new Club();
                $club->setName($data->getAddress());
                $club->setUserLocation($data->getAddress());
                $club->setUuid(Uuid::uuid4()->toString());
            }
            $data->setUserClub($club);
            $club->addUser($data);

            $this->em->persist($club);
        } catch (\Throwable $th) {
            throw new Error("Une erreur est survenue dans la création de l'utilisateur : Impossible de le rattacher à un club");
        }
        
        $this->em->persist($data);
        $this->em->flush();
    }

    public function remove($data)
    {
        $this->em->remove($data);
        $this->em->flush();
    }
}
